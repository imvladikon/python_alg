

def arith_sum_range(n):
    """
    >>> [i for i in arith_sum_range(7)]
    [1, 3, 6, 10, 15, 21, 28]
    """
    k = n
    s = 1
    number = 2
    while k > 0:
        yield s
        k -= 1
        s += number
        number += 1


dict_div = {}
data = [1, 2, 3, 4]
pos = 0
for i, number in enumerate(arith_sum_range(1000)):
    if pos==len(data):
        break

    n = data[pos]
    result = dict_div.get(n)
    # if not result:
        # number_div()
        # dict_div[n] =


