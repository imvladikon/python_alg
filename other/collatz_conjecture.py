
def collatz_range(n):
    """
    >>> [i for i in collatz_range(3)]
    [10, 5, 16, 8, 4, 2, 1]
    >>> [i for i in collatz_range(1)]
    [1]
    """
    k = n
    if k == 1:
        yield 1
    while not k == 1:
        if k % 2 == 0:
            k /= 2
            yield int(k)
        else:
            k *= 3
            k += 1
            yield int(k)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
    for i in collatz_range(1000):
        print(i)